import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:weather_app/WeatherDataList.dart';
import 'dart:convert';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var temp;
  var description;
  var currently;
  var humidity;
  var windSpeed;
  var cityName = '';
  final baseApi = 'https://api.openweathermap.org/data/2.5/weather?q=';
  final apiKey = 'c3c98e6e81504253d74455fa08d26558';
  bool isMetric = true;
  TextEditingController cityNameController = new TextEditingController();

  Future getWeatherData() async {
    http.Response response = await http.get(Uri.parse(isMetric
        ? "${baseApi + cityName + '&units=metric&appid=' + apiKey}"
        : "${baseApi + cityName + '&units=imperial&appid=' + apiKey}"));

    var result = jsonDecode(response.body);
    setState(() {
      this.temp = result['main']['temp'];
      this.description = result['weather'][0]['description'];
      this.currently = result['weather'][0]['main'];
      this.humidity = result['main']['humidity'];
      this.windSpeed = result['wind']['speed'];
    });
  }

  void showDialog() {
    showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: const Text('No city selected '),
            content: const Text('please enter valid city name '),
            actions: [
              CupertinoDialogAction(
                  child: const Text('OK'),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ],
          );
        });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        elevation: 0,
        title: Text('weather app'),
      ),
      drawer: Drawer(
          child: ListView(padding: EdgeInsets.zero, children: [
        DrawerHeader(
          child: Text('Options'),
        ),
        Switch(
          value: isMetric,
          onChanged: (value) {
            setState(() {
              isMetric = value;
              getWeatherData();
            });
          },
        ),
      ])),
      body: Column(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height / 3,
            width: MediaQuery.of(context).size.width,
            color: Colors.red,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Container(
                    height: 40,
                    width: 240,
                    color: Colors.grey[200],
                    child: TextField(
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 18,
                      ),
                      controller: cityNameController,
                      decoration: InputDecoration(
                          suffixIcon: Padding(
                            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                            child: IconButton(
                              iconSize: 22,
                              onPressed: () {
                                cityName = cityNameController.text;
                                setState(() {
                                  cityNameController.text == ''
                                      ? showDialog()
                                      : getWeatherData();
                                });
                              },
                              icon: FaIcon(FontAwesomeIcons.search),
                              color: Colors.red,
                            ),
                          ),
                          hintStyle: TextStyle(
                            fontSize: 18,
                            color: Colors.grey,
                          ),
                          hintText: 'Enter City Name ',
                          prefixIcon: Padding(
                            padding: const EdgeInsets.fromLTRB(5, 8, 0, 0),
                            child: FaIcon(
                              FontAwesomeIcons.mapPin,
                              color: Colors.red,
                            ),
                          ),
                          focusedBorder: InputBorder.none,
                          contentPadding: EdgeInsets.fromLTRB(5, 5, 0, 10)),
                    ),
                  ),
                ]),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: Text(
                    'Currently in $cityName',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w600),
                  ),
                ),
                Text(
                  temp != null ? temp.toString() + '\u00b0' : 'Loading',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: Text(
                    description != null
                        ? description.toString() + '\u00b0'
                        : 'Loading',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ],
            ),
          ),
          WeatherDataList(
            description: description,
            temp: temp,
            humidity: humidity,
            windSpeed: windSpeed,
          )
        ],
      ),
    );
  }
}
