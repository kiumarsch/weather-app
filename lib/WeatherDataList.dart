import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class WeatherDataList extends StatelessWidget {
  final temp;
  final description;
  final humidity;
  final windSpeed;

  const WeatherDataList(
      {Key? key, this.temp, this.description, this.humidity, this.windSpeed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.all(20),
        child: ListView(
          children: <Widget>[
            ListTile(
              leading: FaIcon(FontAwesomeIcons.thermometerHalf),
              title: Text('Temprature'),
              trailing: Text(
                temp != null ? temp.round().toString() + '\u00b0' : 'Loading',
              ),
            ),
            ListTile(
              leading: FaIcon(FontAwesomeIcons.cloud),
              title: Text('weather'),
              trailing: Text(
                description != null ? description.toString() : 'Loading',
              ),
            ),
            ListTile(
              leading: FaIcon(FontAwesomeIcons.sun),
              title: Text('humidity'),
              trailing: Text(
                  humidity != null ? humidity.round().toString() : 'Loading'),
            ),
            ListTile(
              leading: FaIcon(FontAwesomeIcons.wind),
              title: Text('Wind speed '),
              trailing: Text(
                  windSpeed != null ? windSpeed.round().toString() : 'Loading'),
            ),
          ],
        ),
      ),
    );
  }
}
